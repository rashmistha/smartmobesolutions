package com.example.smartmobesolution.Interactor.iInteractor;

import com.example.smartmobesolution.ui.viewModel.UserResponse;

import java.util.List;

public interface UserInteractor {
    void onSuccess(List<UserResponse> userResponseList);

    void onFailure(String message);
}
