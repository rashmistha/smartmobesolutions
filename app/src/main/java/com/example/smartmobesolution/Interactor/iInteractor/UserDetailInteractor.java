package com.example.smartmobesolution.Interactor.iInteractor;

import com.example.smartmobesolution.ui.viewModel.UserResponse;
import com.example.smartmobesolution.ui.viewModel.UserResponseDetail;

import java.util.List;

public interface UserDetailInteractor {
    void onSuccess(UserResponseDetail userResponseDetail);

    void onFailure(String message);
}
