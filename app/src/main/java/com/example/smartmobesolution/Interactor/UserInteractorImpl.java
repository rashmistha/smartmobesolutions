package com.example.smartmobesolution.Interactor;


import android.util.Log;

import com.example.smartmobesolution.Interactor.iInteractor.UserDetailInteractor;
import com.example.smartmobesolution.Interactor.iInteractor.UserInteractor;
import com.example.smartmobesolution.config.api.Api;
import com.example.smartmobesolution.config.api.RetrofitHelper;
import com.example.smartmobesolution.ui.viewModel.UserResponse;
import com.example.smartmobesolution.ui.viewModel.UserResponseDetail;
import com.example.smartmobesolution.utils.Constants.MessageConstants;


import java.util.List;

import androidx.annotation.NonNull;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserInteractorImpl {
    public void getUser(final UserInteractor interactor) {
        final RetrofitHelper helper = RetrofitHelper.getInstance();
        Api service = helper.getService(Api.class);


        Call<List<UserResponse>> call = service.getUsers();
        call.enqueue(new Callback<List<UserResponse>>() {
            @Override
            public void onResponse(@NonNull Call<List<UserResponse>> call, @NonNull Response<List<UserResponse>> response) {
                if (response.code() == 200) {
                    List<UserResponse> userResponses = response.body();
                    interactor.onSuccess(userResponses);
                } else if (response.code() == 400) {
                    interactor.onFailure(response.message());
                } else if (response.code() == 401) {
                    interactor.onFailure("Unauthorize");
                } else if (response.code() == 500) {
                    interactor.onFailure(response.message());
                } else {
                    interactor.onFailure(response.message());
                }

            }

            @Override
            public void onFailure(Call<List<UserResponse>> call, Throwable t) {
                interactor.onFailure(MessageConstants.Networrk_error);
                Log.d("RESPONSE ERROR", t.toString());
            }
        });

    }


    public void getUserDEtail(String username, final UserDetailInteractor interactor) {
        final RetrofitHelper helper = RetrofitHelper.getInstance();
        Api service = helper.getService(Api.class);


        Call<UserResponseDetail> call = service.getUsersDetail(username);
        call.enqueue(new Callback<UserResponseDetail>() {
            @Override
            public void onResponse(@NonNull Call<UserResponseDetail> call, @NonNull Response<UserResponseDetail> response) {
                if (response.code() == 200) {
                    UserResponseDetail userResponses = response.body();
                    interactor.onSuccess(userResponses);
                } else if (response.code() == 400) {
                    interactor.onFailure(response.message());
                } else if (response.code() == 401) {
                    interactor.onFailure("Unauthorize");
                } else if (response.code() == 500) {
                    interactor.onFailure(response.message());
                } else {
                    interactor.onFailure(response.message());
                }

            }

            @Override
            public void onFailure(Call<UserResponseDetail> call, Throwable t) {
                interactor.onFailure(MessageConstants.Networrk_error);
                Log.d("RESPONSE ERROR", t.toString());
            }
        });

    }

}
