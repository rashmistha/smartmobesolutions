package com.example.smartmobesolution.ui.coreUi;

import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import com.example.smartmobesolution.presenter.pInterface.BasePresenter;
import com.example.smartmobesolution.ui.vinterface.IView;
import com.example.smartmobesolution.utils.ProgressDialogFactory;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;


public abstract class BaseActivity<P extends BasePresenter> extends AppCompatActivity implements IView {

    public Context context;
    public P presenter;
    public SwipeRefreshLayout refreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createPresenter();
        setContext();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setBaseRefreshLayout();
    }

    /**
     * Call from onCreate method from all child classes
     */
    public abstract void setContext();

    public abstract void setBaseRefreshLayout();

    @Override
    public void showToast(String message) {
        if (context != null)
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showToast(Integer msg) {
        if (context != null)
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public Context getContext() {
        return context;
    }

    @Override
    public void showLoading(String message) {
        if (refreshLayout != null) {
            if (!refreshLayout.isRefreshing()) {
                refreshLayout.setRefreshing(true);
            }
        } else {
            ProgressDialogFactory.getInstance(context, message).show();
        }
    }

    @Override
    public void hideLoading() {
        if (refreshLayout != null) {
            if (refreshLayout.isRefreshing()) {
                refreshLayout.setRefreshing(false);
            }
        } else {
            ProgressDialogFactory.dismiss();
        }
    }
}
