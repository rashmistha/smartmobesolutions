package com.example.smartmobesolution.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.smartmobesolution.R;
import com.example.smartmobesolution.ui.activity.UserDetailActivity;
import com.example.smartmobesolution.ui.adapter.viewHolder.UserViewHolder;
import com.example.smartmobesolution.ui.viewModel.UserResponse;
import com.example.smartmobesolution.ui.vinterface.IMain;


import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Rashmi on 11/29/2017.
 */

public class UserRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private IMain iMain;
    private LayoutInflater layoutInflater;
    private List<UserResponse> dataList;

    public UserRecyclerAdapter(Context context, List<UserResponse> dataList, IMain iMain) {
        layoutInflater = LayoutInflater.from(context);
        this.dataList = dataList;
        this.iMain = iMain;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final View view = layoutInflater.inflate(R.layout.rv_user_item, parent, false);
        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final UserViewHolder viewHolder = (UserViewHolder) holder;
        viewHolder.txtUserNameRvUserItem.setVisibility(View.VISIBLE);

        viewHolder.imgRvUserItem.setVisibility(View.VISIBLE);
        viewHolder.cdUser.setVisibility(View.VISIBLE);

            viewHolder.txtUserNameRvUserItem.setText(dataList.get(position).getLogin());
        String imageUrl = dataList.get(position).getAvatarUrl();
        if (imageUrl != null) {
            int myVersionAndroid = Build.VERSION.SDK_INT;
            if (myVersionAndroid > Build.VERSION_CODES.M) {
                Glide.with(iMain.getContext())
                        .load(imageUrl)
                        .apply(new RequestOptions()
                                //.fitCenter() // no need for this
                        )
                        .into(viewHolder.imgRvUserItem);
            } else {
                Glide.with(iMain.getContext())
                        .load(imageUrl)
                        .apply(new RequestOptions()
                                //.fitCenter() // no need for this
                        )
                        .into(viewHolder.imgRvUserItem);
            }
        } else {
            viewHolder.imgRvUserItem.setVisibility(View.VISIBLE);

        }


        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(iMain.getContext(), UserDetailActivity.class);
                i.putExtra("username", dataList.get(position).getLogin());
                iMain.getContext().startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
