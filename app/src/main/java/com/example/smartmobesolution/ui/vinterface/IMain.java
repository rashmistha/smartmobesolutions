package com.example.smartmobesolution.ui.vinterface;


import com.example.smartmobesolution.ui.viewModel.UserResponse;

import java.util.List;

public interface IMain extends IView {
    void  getUserList(List<UserResponse> userResponseList);
}
