package com.example.smartmobesolution.ui.activity;

import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.smartmobesolution.R;
import com.example.smartmobesolution.presenter.UserDetailPresenterImpl;
import com.example.smartmobesolution.presenter.pInterface.UserDetailPresenter;
import com.example.smartmobesolution.ui.coreUi.BaseActivity;
import com.example.smartmobesolution.ui.viewModel.UserResponseDetail;
import com.example.smartmobesolution.ui.vinterface.IUserDetail;

public class UserDetailActivity extends BaseActivity<UserDetailPresenter> implements IUserDetail {
    TextView txtName;
    TextView txtEmail;
    TextView txtLocation;
    TextView txtCompany;
    TextView txtfollower;
    TextView txtfollowing;
    TextView txtBlog;

    ImageView imgAvatar;
    LinearLayout liFollower;
    LinearLayout liFollowing;
    //ActivityMainBinding binding;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_detail);

        txtName = findViewById(R.id.txt_un_d_name);
        txtEmail = findViewById(R.id.txt_un_d_email);
        txtLocation = findViewById(R.id.txt_un_d_location);
        txtCompany = findViewById(R.id.txt_un_d_company);
        txtfollower = findViewById(R.id.txt_un_d_followers);
        txtfollowing = findViewById(R.id.txt_un_d_following);
        txtBlog = findViewById(R.id.txt_un_d_blog);
        imgAvatar = findViewById(R.id.img_un_d_avatar);
        liFollower=findViewById(R.id.li_follower);
        liFollowing=findViewById(R.id.li_following);




        String username = getIntent().getStringExtra("username");
        presenter.onUserDetail(username);
    }

    @Override
    public void setContext() {
        context = this;
    }

    @Override
    public void setBaseRefreshLayout() {
        refreshLayout = null;
    }

    @Override
    public void getUserDetail(UserResponseDetail userResponseDetail) {


        String imageUrl = userResponseDetail.getAvatarUrl();
        if (imageUrl != null) {
            int myVersionAndroid = Build.VERSION.SDK_INT;
            if (myVersionAndroid > Build.VERSION_CODES.M) {
                Glide.with(getContext())
                        .load(imageUrl)
                        .apply(new RequestOptions()
                                //.fitCenter() // no need for this
                        )
                        .into(imgAvatar);
            } else {
                Glide.with(getContext())
                        .load(imageUrl)
                        .apply(new RequestOptions()
                                //.fitCenter() // no need for this
                        )
                        .into(imgAvatar);
            }
        } else {
            imgAvatar.setVisibility(View.VISIBLE);

        }

        if (userResponseDetail.getFollowers() != null) {
            txtfollower.setText(userResponseDetail.getFollowers().toString());
            txtfollower.setVisibility(View.VISIBLE);
            liFollower.setVisibility(View.VISIBLE);

        } else {
            txtfollower.setVisibility(View.GONE);
        }
        if (userResponseDetail.getFollowing() != null) {
            txtfollowing.setText(userResponseDetail.getFollowing().toString());
            txtfollowing.setVisibility(View.VISIBLE);
            liFollowing.setVisibility(View.VISIBLE);
        } else {
            txtfollowing.setVisibility(View.GONE);
        }
        if (userResponseDetail.getName() != null) {
            txtName.setText(userResponseDetail.getName());
            txtName.setVisibility(View.VISIBLE);
        } else {
            txtName.setVisibility(View.GONE);
        }
        if (userResponseDetail.getEmail() != null) {
            txtEmail.setText(userResponseDetail.getEmail());
            txtEmail.setVisibility(View.VISIBLE);
        } else {
            txtEmail.setVisibility(View.GONE);
        }
        if (userResponseDetail.getLocation() != null) {
            txtLocation.setText(userResponseDetail.getLocation());
            txtLocation.setVisibility(View.VISIBLE);
        } else {
            txtLocation.setVisibility(View.GONE);
        }
        if (userResponseDetail.getCompany() != null) {
            txtCompany.setText(userResponseDetail.getCompany());
            txtCompany.setVisibility(View.VISIBLE);
        } else {
            txtCompany.setVisibility(View.GONE);
        }
        if (userResponseDetail.getBlog() != null) {
            // txtBlog.setText(userResponseDetail.getBlog());
        /*    txtBlog.setText(Html.fromHtml(userResponseDetail.getBlog()));*/
        /*    txtBlog.setText(
                    Html.fromHtml("<a href=\"http://www.google.com\">"+userResponseDetail.getBlog()+"</a> "));*/

            txtBlog.setText(Html.fromHtml("<a href=\""+ userResponseDetail.getBlog() + "\">" + userResponseDetail.getBlog() + "</a>"));
            txtBlog.setMovementMethod(LinkMovementMethod.getInstance());

        } else {
            txtBlog.setVisibility(View.GONE);
        }
    }

    @Override
    public void createPresenter() {
        presenter = new UserDetailPresenterImpl(this);
    }
}
