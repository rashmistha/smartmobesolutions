package com.example.smartmobesolution.ui.adapter.viewHolder;


import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.smartmobesolution.R;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class UserViewHolder extends RecyclerView.ViewHolder {
    // @BindView(R.id.img_rv_user_item)
    public ImageView imgRvUserItem;
    //   @BindView(R.id.txt_username_rv_user_item)
    public TextView txtUserNameRvUserItem;
    public CardView cdUser;

    public UserViewHolder(@NonNull View itemView) {
        super(itemView);

        imgRvUserItem = itemView.findViewById(R.id.img_rv_user_item);
        txtUserNameRvUserItem = itemView.findViewById(R.id.txt_username_rv_user_item);
        cdUser = itemView.findViewById(R.id.cd_user);
        //    ButterKnife.bind(this, itemView);
    }
}
