package com.example.smartmobesolution.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.smartmobesolution.R;
import com.example.smartmobesolution.presenter.MainPresenterImpl;
import com.example.smartmobesolution.presenter.pInterface.MainPresenter;
import com.example.smartmobesolution.ui.adapter.UserRecyclerAdapter;
import com.example.smartmobesolution.ui.coreUi.BaseActivity;
import com.example.smartmobesolution.ui.viewModel.UserResponse;
import com.example.smartmobesolution.ui.vinterface.IMain;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity<MainPresenter> implements IMain {
    /*@BindView(R.id.rv_user)*/
    RecyclerView rvUser;
    List<UserResponse> userResponseList1;
   // private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rvUser = findViewById(R.id.rv_user);
        // ButterKnife.bind(this);
        presenter.onUser();
    }

    @Override
    public void setContext() {
        context = this;
    }

    @Override
    public void setBaseRefreshLayout() {
        refreshLayout = null;
    }

    @Override
    public void createPresenter() {
        presenter = new MainPresenterImpl(this);
    }

    @Override
    public void getUserList(List<UserResponse> userResponseList) {
        userResponseList1 = new ArrayList<>();
        userResponseList1 = userResponseList;
        UserRecyclerAdapter subscriptionAdapter = new UserRecyclerAdapter(getContext(), userResponseList, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rvUser.setLayoutManager(linearLayoutManager);
        rvUser.setItemAnimator(new DefaultItemAnimator());
        rvUser.setAdapter(subscriptionAdapter);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        MenuItem mSearch = menu.findItem(R.id.action_search);


        androidx.appcompat.widget.SearchView mSearchView = (SearchView) mSearch.getActionView();

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                List<UserResponse> searchList = new ArrayList<>();
                int searchListLength = userResponseList1.size();
                for (int i = 0; i < searchListLength; i++) {
                    if (userResponseList1.get(i).getLogin().contains(newText)) { //Do whatever you want here } }
                        searchList.add(userResponseList1.get(i));
                    }
                }

                getUserSearchList(searchList);
                return true;
            }
        });

/*
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {



                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                List<UserResponse> searchList = new ArrayList<>();
                int searchListLength = userResponseList1.size();
                for (int i = 0; i < searchListLength; i++) {
                    if (userResponseList1.get(i).getLogin().contains(newText)) { //Do whatever you want here } }
                        searchList.add(userResponseList1.get(i));
                    }
                }

                getUserSearchList(searchList);
                return true;
            }
        });*/


        return super.onCreateOptionsMenu(menu);


    }

    public void getUserSearchList(List<UserResponse> userResponseList) {
        UserRecyclerAdapter subscriptionAdapter = new UserRecyclerAdapter(getContext(), userResponseList, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rvUser.setLayoutManager(linearLayoutManager);
        rvUser.setItemAnimator(new DefaultItemAnimator());
        rvUser.setAdapter(subscriptionAdapter);

    }

}
