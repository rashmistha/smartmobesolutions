package com.example.smartmobesolution.ui.vinterface;


import com.example.smartmobesolution.ui.viewModel.UserResponse;
import com.example.smartmobesolution.ui.viewModel.UserResponseDetail;

import java.util.List;

public interface IUserDetail extends IView {
    void  getUserDetail(UserResponseDetail userResponseDetail);


}
