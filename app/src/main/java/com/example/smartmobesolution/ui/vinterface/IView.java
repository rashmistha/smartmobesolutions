package com.example.smartmobesolution.ui.vinterface;

import android.content.Context;

/**
 * Created by Rashmi on 11/26/2017.
 */

public interface IView {
    void showToast(String message);
    void showToast(Integer msg);

    void showLoading(String message);

    void hideLoading();

    void createPresenter();

    Context getContext();
}
