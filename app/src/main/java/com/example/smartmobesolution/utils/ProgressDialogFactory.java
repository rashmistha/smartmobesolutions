package com.example.smartmobesolution.utils;


import android.app.ProgressDialog;
import android.content.Context;

import com.example.smartmobesolution.R;


public class ProgressDialogFactory {
    private static ProgressDialogFactory instance = null;
    private static ProgressDialog progressDialog;

    private ProgressDialogFactory(Context context, String message) {
        progressDialog = new ProgressDialog(context, R.style.ProgressDialogStyle);
        progressDialog.setMessage(message);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
    }

    public static ProgressDialogFactory getInstance(Context context, String msg) {
        if (instance == null) {
            instance = new ProgressDialogFactory(context, msg);
        }
        return instance;
    }

    public static void dismiss() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        progressDialog = null;
        instance = null;
    }

    public void show() {
        if (progressDialog != null && !progressDialog.isShowing()) {
            progressDialog.show();
        }
    }
}

