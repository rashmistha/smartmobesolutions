package com.example.smartmobesolution.config.api;


import com.example.smartmobesolution.ui.viewModel.UserResponse;
import com.example.smartmobesolution.ui.viewModel.UserResponseDetail;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Rashmi on 3/19/2018.
 */

public interface Api {
    @GET("users")
    Call<List<UserResponse>> getUsers();

    @GET("users/{username}")
    Call<UserResponseDetail> getUsersDetail(@Path("username") String username);

}
