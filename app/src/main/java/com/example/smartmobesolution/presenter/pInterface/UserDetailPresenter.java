package com.example.smartmobesolution.presenter.pInterface;


public interface UserDetailPresenter extends BasePresenter {
    void onUserDetail(String username);

}
