package com.example.smartmobesolution.presenter;


import com.example.smartmobesolution.presenter.pInterface.BasePresenter;
import com.example.smartmobesolution.ui.vinterface.IView;

/**
 * Created by Rashmi on 11/26/2017.
 */

public class BasePresenterImpl<V extends IView> implements BasePresenter {
    private V iView;

    public BasePresenterImpl(V iView) {
        this.iView = iView;
    }

    @Override

    public V getView() {
        return iView;
    }
}
