package com.example.smartmobesolution.presenter;


import com.example.smartmobesolution.Interactor.UserInteractorImpl;
import com.example.smartmobesolution.Interactor.iInteractor.UserInteractor;
import com.example.smartmobesolution.presenter.pInterface.MainPresenter;
import com.example.smartmobesolution.ui.viewModel.UserResponse;
import com.example.smartmobesolution.ui.vinterface.IMain;
import com.example.smartmobesolution.utils.NetworkUtils;

import java.util.List;

public class MainPresenterImpl extends BasePresenterImpl<IMain> implements MainPresenter {
    public MainPresenterImpl(IMain iView) {
        super(iView);
    }


    @Override
    public void onUser() {
      //  getView().showLoading("Loading...");
        if (!NetworkUtils.isConnected(getView().getContext())) {
            NetworkUtils.noInternetConnection(getView().getContext());
            return;
        }
        UserInteractorImpl interactor = new UserInteractorImpl();
        interactor.getUser(new UserInteractor() {

            @Override
            public void onSuccess(List<UserResponse> userResponseList) {
                if (getView() != null) {
                    getView().getUserList(userResponseList);
                    getView().hideLoading();
                }

            }

            @Override
            public void onFailure(String message) {
                if (getView() != null) {
                    getView().showToast(message);
                    getView().hideLoading();
                }
            }

        });
    }
}
