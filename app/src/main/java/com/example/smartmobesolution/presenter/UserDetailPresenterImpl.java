package com.example.smartmobesolution.presenter;


import com.example.smartmobesolution.Interactor.UserInteractorImpl;
import com.example.smartmobesolution.Interactor.iInteractor.UserDetailInteractor;
import com.example.smartmobesolution.Interactor.iInteractor.UserInteractor;
import com.example.smartmobesolution.presenter.pInterface.MainPresenter;
import com.example.smartmobesolution.presenter.pInterface.UserDetailPresenter;
import com.example.smartmobesolution.ui.viewModel.UserResponse;
import com.example.smartmobesolution.ui.viewModel.UserResponseDetail;
import com.example.smartmobesolution.ui.vinterface.IMain;
import com.example.smartmobesolution.ui.vinterface.IUserDetail;
import com.example.smartmobesolution.utils.NetworkUtils;

import java.util.List;

public class UserDetailPresenterImpl extends BasePresenterImpl<IUserDetail> implements UserDetailPresenter {
    public UserDetailPresenterImpl(IUserDetail iView) {
        super(iView);
    }


    @Override
    public void onUserDetail(String username) {
        if (!NetworkUtils.isConnected(getView().getContext())) {
            NetworkUtils.noInternetConnection(getView().getContext());
            return;
        }

        UserInteractorImpl interactor = new UserInteractorImpl();
        interactor.getUserDEtail(username, new UserDetailInteractor() {


            @Override
            public void onSuccess(UserResponseDetail userResponseDetail) {
                getView().getUserDetail(userResponseDetail);
                getView().hideLoading();


            }

            @Override
            public void onFailure(String message) {
                if (getView() != null) {
                    getView().showToast(message);
                    getView().hideLoading();
                }
            }
        });
    }
}
