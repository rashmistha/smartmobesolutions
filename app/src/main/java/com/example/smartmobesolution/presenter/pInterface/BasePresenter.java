package com.example.smartmobesolution.presenter.pInterface;


import com.example.smartmobesolution.ui.vinterface.IView;

/**
 * Created by Rashmi on 11/26/2017.
 */

public interface BasePresenter {
    IView getView();
}
